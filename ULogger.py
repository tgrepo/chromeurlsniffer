from flask import Flask
from flask import request
import datetime
import os

WDIR = os.path.dirname(__file__)
LDIR = os.path.join(WDIR, 'log')

class UrlLogger:

    def __init__(self):
        pass

    def log(self, url):
        print(url)
        cur_datetime = datetime.datetime.now()
        date = cur_datetime.strftime("%Y_%m_%d")
        time = cur_datetime.strftime("[%H:%M:%S]: ") 
 #       src, url = tuple(url.split())
        #mkdir_on_need(src)
        fname = os.path.join(LDIR, date + '.txt')
    
        with open(fname, 'a') as f:
            print(time + url, file=f)

app = Flask(__name__)
logger = UrlLogger()

@app.route("/", methods=['POST'])
def hello():
    logger.log(request.data.decode("utf-8"))
    return ''

if __name__ == "__main__":
    app.run()

