<# :
@echo off
copy/b "%~f0" "%temp%\%~n0.ps1" >nul
powershell -v 2 -ep bypass -noprofile "%temp%\%~n0.ps1" "'%cd% '" "'%~1'"
del "%temp%\%~n0.ps1"
echo: & pause
exit /b
#>

# HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Update\AutoUpdateCheckPeriodMinutes to the REG_DWORD value of "0"

$registryPath = "HKLM:\SOFTWARE\Policies\Google\Update"
$Name = "AutoUpdateCheckPeriodMinutes"
$value = "0"

mkdir -Name Google -Path HKLM:\SOFTWARE\Policies -Force
mkdir -Name Update -Path HKLM:\SOFTWARE\Policies\Google -Force
New-ItemProperty -Path $registryPath -Name $name -Value $value -PropertyType DWORD -Force




